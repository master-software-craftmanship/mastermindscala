package models

class MasterMindRow(__line: List[Char]) {
  private val _line = __line

  def numberOfCorrectColorAndPositionWith(masterMindRow: MasterMindRow): Int =
    _line.zip(masterMindRow._line).map( { case (a, b) => a==b }).count(_ == true)

  def numberOfCorrectColorButNotPositionWith(masterMindRow: MasterMindRow): Int = {
    val (row1, row2) = removeSamePositionSameColorChars(_line, masterMindRow._line)
    numberOfRepeatedElements(row1, row2)
  }

  def removeCharFromList(c: Char, list: List[Char]): (Int, List[Char]) = {
    list match {
      case Nil => (0, Nil)
      case header::tail if header == c => (1, tail)
      case header::tail => {
        val (f, fixed) = removeCharFromList(c, tail)
        (f, header::fixed)
      }
    }
  }

  def removeSamePositionSameColorChars(a: List[Char], b: List[Char]): (List[Char], List[Char]) = {
    a.zip(b).filter{case (x, y) => x != y }.unzip
  }

  def numberOfRepeatedElements(a: List[Char], b: List[Char]): Int = {
    a match {
      case Nil => 0
      case header::tail => {
        val (found, remaining) = removeCharFromList(header, b)
        found + numberOfRepeatedElements(tail, remaining)
      }
    }
  }

  def canEqual(other: Any): Boolean = other.isInstanceOf[MasterMindRow]

  override def equals(other: Any): Boolean = other match {
    case that: MasterMindRow =>
      (that canEqual this) &&
        _line == that._line
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(_line)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }

  override def toString = _line.toString()
}
