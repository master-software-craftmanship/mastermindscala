package models

class Game(__guess: MasterMindRow, __inputGuesses: List[MasterMindRow] = List[MasterMindRow]()) {
  private val _guess = __guess
  private val _inputGuesses = __inputGuesses

  def guess: MasterMindRow =
    _guess

  def putRow(row: MasterMindRow): Game =
    new Game(_guess, row::_inputGuesses)

  def guessResponse(row: MasterMindRow): List[Char] = {
    val numberOfCorrect = _guess.numberOfCorrectColorAndPositionWith(row)
    val numberOfCuasiCorrect = _guess.numberOfCorrectColorButNotPositionWith(row)
    List.fill(numberOfCorrect)('n'):::List.fill(numberOfCuasiCorrect)('b')
  }

  def gameEnds: Boolean =
    _inputGuesses.length == 15 || playerWins

  def playerWins: Boolean =
    _inputGuesses.length > 0 && _guess.numberOfCorrectColorAndPositionWith(_inputGuesses.head) == 4

  override def toString = toString2(_inputGuesses)

  def toString2(rows: List[MasterMindRow]): String = {
    rows match {
      case Nil => ""
      case row::tail => {
        val response = guessResponse(row)
        s"$response\t$row\n" + toString2(tail)
      }
    }
  }
}
