package views

import models.MasterMindRow

object GuessView {
  def read: MasterMindRow = {
    GestorIO.write("escribe combinacion")
    var fichas:Array[Char] = new Array[Char](4)
    for(i: Int <- 0 to 3) {
      fichas.update(i, GestorIO.readChar(""))
    }
    new MasterMindRow(fichas.toList)
  }
}
