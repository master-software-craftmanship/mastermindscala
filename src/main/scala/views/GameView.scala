package views

import models.Game

object GameView {
  def write(game: Game): Unit = {
    GestorIO.write(s"Game:\n" + game.toString)
  }
}
