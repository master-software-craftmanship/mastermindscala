package views

object GestorIO {
  def readChar(message: String): Char = {
    if (message.length > 0) {
      println(message)
    }
    scala.io.StdIn.readChar()
  }

  def readString(message: String): String =
    scala.io.StdIn.readLine(message)

  def readInt(message: String): Int =
    readString(message).toInt

  def write(message: String): Unit =
    println(message)
}
