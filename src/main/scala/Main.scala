import models.Game
import views.{GameView, GuessView}

object Main {
  def main(args: Array[String]): Unit = {
    val guess = GuessView.read
    var game = new Game(guess)
    while (!game.gameEnds) {
      val inputGuess = GuessView.read
      game = game.putRow(inputGuess)
      GameView.write(game)
    }
  }

}
